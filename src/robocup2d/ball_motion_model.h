#ifndef BALL_MOTION_MODEL_H
#define BALL_MOTION_MODEL_H

#include <iball_motion_model.h>

namespace soccer {

class BallMotionModel
        : public IBallMotionModel
{
public:
    BallMotionModel();

    Ball freeMotion(const Ball& ball, unsigned int time);
    Ball freeMotion(const Ball& ball);

};

}

#endif // BALL_MOTION_MODEL_H
