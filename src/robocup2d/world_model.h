#ifndef WORLD_MODEL_H
#define WORLD_MODEL_H

#include <rcsc/geom.h>

#include <map>
#include <types.h>

namespace soccer {
namespace robocup2d {


class WorldModel
{

private:
    WorldModel();


public:
    static WorldModel& instance();
    ~WorldModel();

    const rcsc::Rect2D& field();
    const rcsc::Rect2D& midfield(Side side);
    const rcsc::Rect2D& penaltyArea(Side side);
    const rcsc::Rect2D& goalieArea(Side side);
    const rcsc::Rect2D& goal(Side side);

    const rcsc::Circle2D& centerSpot();
    const rcsc::Circle2D& centerCircle();
    const rcsc::Circle2D& penaltySpot(Side side);

    const rcsc::Region2D& region(const std::string& name);

    const rcsc::Line2D& endLine(Side side);
    const rcsc::Line2D& sideLine(Side side);
    const rcsc::Line2D& goalLine(Side side);
    const rcsc::Line2D& midfieldLine();
private:
    // Regions
    rcsc::Rect2D M_field;
    rcsc::Rect2D M_left_midfield;
    rcsc::Rect2D M_right_midfield;
    rcsc::Rect2D M_left_penalty_area;
    rcsc::Rect2D M_right_penalty_area;
    rcsc::Rect2D M_left_goalie_area;
    rcsc::Rect2D M_right_goalie_area;
    rcsc::Rect2D M_left_goal;
    rcsc::Rect2D M_right_goal;
    rcsc::Circle2D M_left_penalty_spot;
    rcsc::Circle2D M_right_penalty_spot;
    rcsc::Circle2D M_center_spot;
    rcsc::Circle2D M_center_circle;

    std::map<std::string, rcsc::Region2D*> M_regions;

    // Lines

    std::map<std::string, rcsc::Line2D*> M_lines;


};

}
}

#endif
