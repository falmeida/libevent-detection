#ifndef GOAL_OPPORTUNIY_DETECTOR_H
#define GOAL_OPPORTUNIY_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsslogplayer/types.h>

namespace soccer
{
	namespace event
	{

                class GoalOpportunityDetector
                        : public IObservable<IEvent>
                        , public IObserver<rcss::rcg::DispInfoT> {
		public:
			GoalOpportunityDetector();
			
                        void update(rcss::rcg::DispInfoT& scene);
			
                };
        }
}

#endif 
