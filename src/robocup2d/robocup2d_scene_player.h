#ifndef ROBOCUP2D_SCENE_PLAYER
#define ROBOCUP2D_SCENE_PLAYER

// -*-c++-*-

/***************************************************************************
                                soccer_scene_parser.cpp
                         Parse soccer scenes from robocup 2D log files
                             -------------------
    begin                : 26-SEP-2012
    copyright            : (C) 2012 by Fernando Almeida.
    email                : falmeida@di.estv.ipv.pt
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU LGPL as published by the Free Software  *
 *   Foundation; either version 2 of the License, or (at your option) any  *
 *   later version.                                                        *
 *                                                                         *
 ***************************************************************************/

#include <rcsslogplayer/parser.h>
#include <rcsslogplayer/handler.h>
#include <rcsslogplayer/gzfstream.h>

#include <map>
#include <sstream>
#include <fstream>
#include <iostream>
#include <limits>
#include <cstring>
#include <cmath>

#include <vector>

#include <scene_player.h>

using namespace std;


namespace rcss {
namespace rcg {

class RoboCup2DScenePlayer
    : public rcss::rcg::Handler
    , public soccer::ScenePlayer<DispInfoT>
{
private:
    RoboCup2DScenePlayer();

    ~RoboCup2DScenePlayer();
public:

    static RoboCup2DScenePlayer& instance();

    static rcss::rcg::PlayMode
    play_mode_id( const char * mode );



    /// OVERRIDE soccer::ScenePlayer interface

    void stop();
    void pause();
    void play();

    const DispInfoT* forward(unsigned int step = 1);
    const DispInfoT* rewind(unsigned int step = 1);
    void goToScene(unsigned int index);

    const DispInfoT* currentScene() const;

    unsigned int currentindex() const;
    bool isPaused() const;
    bool isLastScene() const;
    bool isFirstScene() const;
    bool hasScene(unsigned int index) const;

    const ServerParamT& serverParams() const;
    const PlayerParamT& playerParams() const;
    const PlayerTypeT& playerType(unsigned int type) const;

private:

    void doHandleLogVersion( int ver );

    int doGetLogVersion() const;

    void doHandleEOF();

    void doHandleServerParam( const ServerParamT & param );

    void doHandlePlayerParam( const PlayerParamT & param );


    void doHandlePlayerType( const PlayerTypeT & type );

    void updateTime( int time );

    void doHandleShowInfo( const ShowInfoT & info );

    void doHandleMsgInfo( const int,
                          const int ,
                          const string & ) { }

    void doHandlePlayMode( const int,
                           const PlayMode pmode );

    void doHandleTeamInfo( const int,
                           const TeamT & left,
                           const TeamT & right );


    virtual
    void doHandleDrawClear( const int )
      { }

    virtual
    void doHandleDrawPointInfo( const int,
                                const PointInfoT & )
      { }

    virtual
    void doHandleDrawCircleInfo( const int,
                                 const CircleInfoT & )
      { }

    virtual
    void doHandleDrawLineInfo( const int,
                               const LineInfoT & )
      { }


private:
    /// Log version
    int M_version;

    /// Current time
    int M_time;

    /// Current stopped time
    int M_stopped_time;

    /// Total game time, including stopped time
    unsigned int M_total_time;

    /// Current playmode
    PlayMode M_pmode;
    TeamT M_team_left;
    TeamT M_team_right;

    /// Log filename
    string M_game_log_filename;

    /// Simulation parameters
    ServerParamT M_server_params;
    PlayerParamT M_player_params;
    map<int, PlayerTypeT> M_player_types;

    /// Members to control the soccer::ScenePlayer interface
    std::vector<const DispInfoT*> M_scenes;

    unsigned int M_current_scene_index;

    bool M_paused;
    bool M_stopped;
};

} // end namespace rcg
} // end namespace rcss

#endif

