#ifndef BALL_DISPUTE_DETECTOR_H
#define BALL_DISPUTE_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>
#include <ievent.h>

#include <rcsc/rcg/types.h>

#include <vector>
#include <ball_kick.h>
#include <ball_dispute.h>

namespace soccer {
namespace event {

    class BallDisputeDetector
            : public IObservable<BallDispute>
            , public IObserver<BallKick>
            , public IObserver<rcsc::rcg::DispInfoT>
    {
    public:
        BallDisputeDetector();


        void update(const rcsc::rcg::DispInfoT& scene);
        void update(const BallKick& ball_kick);

    private:
        std::vector<BallKick> M_ball_kicks;
    };

}
}

#endif
