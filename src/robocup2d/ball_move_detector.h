#ifndef BALL_MOVE_DETECTOR_H
#define BALL_MOVE_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsslogplayer/types.h>

namespace soccer
{
	namespace event
	{

                class BallMoveDetector
                        : public IObservable<IEvent>
                        , public IObserver<rcss::rcg::DispInfoT> {
		public:
			BallMoveDetector(double min_move_distance);
					   
            void update(const rcss::rcg::DispInfoT& scene);
		private:
			double M_min_move_distance;
		};
	
	}
}

#endif
