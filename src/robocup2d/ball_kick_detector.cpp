#include "ball_kick_detector.h"

#include <vector>
#include <rcsc/geom.h>

#include "robocup2d_scene_player.h"

#include <ball_kick.h>

using namespace std;
using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer {
namespace event {


BallKickDetector::BallKickDetector()
    : M_last_scene(NULL)
{

}

void BallKickDetector::update(const DispInfoT& scene)
{
    vector<PlayerId> kickers;

    if ( M_last_scene != NULL )
    {
        const BallT& last_ball = M_last_scene->show_.ball_;
        const BallT& ball = scene.show_.ball_;
        for ( unsigned int p = 0; p < 22; p++)
        {
            const PlayerT& last_player = M_last_scene->show_.player_[p];
            const PlayerT& curr_player = scene.show_.player_[p];

            if ( canKick(last_player, last_ball) && curr_player.isKicking() ||
                 canCatch(last_player, last_ball) && curr_player.isCatching() ||
                 canTackle(last_player, last_ball) && curr_player.isTackling())
            {
                kickers.push_back(PlayerId(last_player.unum_,
                                           last_player.side() == LEFT ? SIDE_LEFT : SIDE_RIGHT));
            }
        }

        if ( !kickers.empty() )
        {
            for ( vector<PlayerId>::const_iterator cit = kickers.begin();
                  cit != kickers.end();
                  cit++)
            {
                BallKick ball_kick(M_last_scene->show_.time_, *cit);
                notifyObservers( ball_kick );
            }
        }
    }

    M_last_scene = &scene;
}

bool BallKickDetector::canKick(const PlayerT& player,
                               const BallT& ball)
{
    Vector2D player_position(player.x_, player.y_);
    Vector2D ball_position(ball.x_, ball.y_);

    const rcss::rcg::ServerParamT& SP = rcss::rcg::RoboCup2DScenePlayer::instance().serverParams();
    const rcss::rcg::PlayerTypeT& PT = rcss::rcg::RoboCup2DScenePlayer::instance().playerType(player.type_);

    return player_position.dist(ball_position) <= (SP.ball_size_ +
                                                   PT.player_size_ +
                                                   PT.kickable_margin_);
}

bool BallKickDetector::canCatch(const PlayerT& player,
                                const BallT& ball)
{
    Vector2D player_position(player.x_, player.y_);
    Vector2D ball_position(ball.x_, ball.y_);

    const rcss::rcg::PlayerTypeT& PT = rcss::rcg::RoboCup2DScenePlayer::instance().playerType(player.type_);
    const rcss::rcg::ServerParamT& SP = rcss::rcg::RoboCup2DScenePlayer::instance().serverParams();

    return player_position.dist(ball_position) <= (SP.catchable_area_l_ +
                                                   PT.catchable_area_l_stretch_);
}

bool BallKickDetector::canTackle(const PlayerT& player,
                                 const BallT& ball)
{
    Vector2D player_position(player.x_, player.y_);
    Vector2D ball_position(ball.x_, ball.y_);

    const rcss::rcg::ServerParamT& SP = rcss::rcg::RoboCup2DScenePlayer::instance().serverParams();

    Vector2D player_relative_to_ball = ball_position - player_position;
    player_relative_to_ball.rotate(-player.body_);

    return (player_relative_to_ball.x > 0 &&
            player_relative_to_ball.x <= SP.tackle_dist_ &&
            abs(player_relative_to_ball.y) <= SP.tackle_width_ / 2);

}

}
}
