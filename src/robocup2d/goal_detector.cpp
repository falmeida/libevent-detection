#include "goal_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer
{
    namespace event
    {

        GoalDetector::GoalDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void GoalDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_AfterGoal_Left:
                {
                        Goal goal_left(scene.show_.time_, SIDE_LEFT);
                        notifyObservers( goal_left );
                 }
                    break;
                    case PM_AfterGoal_Right:
                {
                        Goal goal_right(scene.show_.time_, SIDE_RIGHT);
                        notifyObservers( goal_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }
	}
}
