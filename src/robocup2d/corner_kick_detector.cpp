#include "corner_kick_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer 
{
	namespace event 
	{

        CornerKickDetector::CornerKickDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
		{

		}

        void CornerKickDetector::update(const DispInfoT& scene)
		{
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_CornerKick_Left:
                {
                        CornerKick corner_kick_left(M_last_playmode_time
                                                    , scene.show_.time_
                                                    , SIDE_LEFT);
                        notifyObservers( corner_kick_left );
                 }
                    break;
                    case PM_CornerKick_Right:
                {
                        CornerKick corner_kick_right(M_last_playmode_time
                                                     , scene.show_.time_
                                                     , SIDE_RIGHT);
                        notifyObservers( corner_kick_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }

	}
}
