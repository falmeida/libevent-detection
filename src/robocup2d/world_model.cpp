#include "world_model.h"

#include <algorithm>

using namespace std;
using namespace rcsc;

namespace soccer {
namespace robocup2d {

WorldModel& WorldModel::instance()
{
    static WorldModel instance;
    return instance;
}

WorldModel::WorldModel()
{
    // Initialize regions
    Rect2D* field = new Rect2D( Vector2D(-52.5, -34.0), Size2D(105, 68.0) );
    Rect2D* left_midfield = new Rect2D( Vector2D(field->left(), -34.0)  , Size2D(52.5 , 68.0));
    Rect2D* right_midfield = new Rect2D( Vector2D(0.0, -34.0)  , Size2D(52.5 , 68.0));
    Rect2D* left_penalty_area = new Rect2D( Vector2D(field->left(), -20.16) , Size2D(16.5 , 40.32));
    Rect2D* right_penalty_area = new Rect2D(Vector2D(field->right() - 16.5 , -20.16) , Size2D(16.5 , 40.32));
    Rect2D* left_goalie_area = new Rect2D( Vector2D(field->left()          , -9.16)  , Size2D(5.5  , 18.32));
    Rect2D* right_goalie_area = new Rect2D( Vector2D(field->right() - 5.5   , -9.16)  , Size2D(5.5  , 18.32));
    Rect2D* left_goal = new Rect2D( Vector2D(field->left() - 2.44, -7.01) , Size2D(2.44, 14.02));
    Rect2D* right_goal = new Rect2D( Vector2D(field->right(), -7.01), Size2D(2.44, 14.02));
    Circle2D* left_penalty_spot = new Circle2D( Vector2D(field->left() + 11.0, 0.0), 0.5);
    Circle2D* right_penalty_spot = new Circle2D( Vector2D(field->right() - 11.0, 0.0), 0.5);
    Circle2D* center_spot = new Circle2D( Vector2D(0.0, 0.0), 0.5);
    Circle2D* center_circle = new Circle2D( Vector2D(0.0, 0.0), 9.15);

    M_regions.insert(pair<string, Region2D*>("field", field));
    M_regions.insert(pair<string, Region2D*>("left_midfield", left_midfield));
    M_regions.insert(pair<string, Region2D*>("right_midfield", right_midfield));
    M_regions.insert(pair<string, Region2D*>("left_penalty_area", left_penalty_area));
    M_regions.insert(pair<string, Region2D*>("right_penalty_area", right_penalty_area));
    M_regions.insert(pair<string, Region2D*>("left_goalie_area", left_goalie_area));
    M_regions.insert(pair<string, Region2D*>("right_goalie_area", right_goalie_area));
    M_regions.insert(pair<string, Region2D*>("left_goal", left_goal));
    M_regions.insert(pair<string, Region2D*>("right_goal", right_goal));
    M_regions.insert(pair<string, Region2D*>("left_penalty_spot", left_penalty_spot));
    M_regions.insert(pair<string, Region2D*>("right_penalty_spot", right_penalty_spot));
    M_regions.insert(pair<string, Region2D*>("center_spot", center_spot));
    M_regions.insert(pair<string, Region2D*>("center_circle", center_circle));
}

WorldModel::~WorldModel()
{
    for ( map<string,Region2D*>::iterator it = M_regions.begin();
          it != M_regions.end();
          it++)
    {
        delete it->second;
    }
}

const rcsc::Rect2D& WorldModel::field()
{
    return static_cast<const Rect2D&>(region("field"));
}

const rcsc::Rect2D& WorldModel::penaltyArea(Side side)
{
    return static_cast<const Rect2D&>(side == SIDE_LEFT ? region("left_penalty_area") : region("right_penalty_area"));
}

const rcsc::Rect2D& WorldModel::goalieArea(Side side)
{
    return static_cast<const Rect2D&>(side == SIDE_LEFT ? region("left_goalie_area") : region("right_goalie_area"));
}
const rcsc::Rect2D& WorldModel::goal(Side side)
{
    return static_cast<const Rect2D&>(side == SIDE_LEFT ? region("left_goal") : region("right_goal"));
}

const rcsc::Circle2D& WorldModel::centerSpot()
{
    return static_cast<const Circle2D&>(region("center_spot"));
}

const rcsc::Circle2D& WorldModel::centerCircle()
{
    return static_cast<const Circle2D&>(region("center_circle"));
}

const rcsc::Circle2D &WorldModel::penaltySpot(Side side)
{
    return static_cast<const Circle2D&>(side == SIDE_LEFT ? region("left_penalty_spot") : region("right_penalty_spot"));
}

const rcsc::Rect2D& WorldModel::midfield(Side side)
{
    return static_cast<const Rect2D&>(side == SIDE_LEFT ? region("left_midfield") : region("right_midfield"));
}

const rcsc::Region2D& WorldModel::region(const std::string& name)
{
    assert ( !M_regions.empty() );

    map<string, Region2D*>::const_iterator it = M_regions.find(name);
    assert ( it != M_regions.end() );

    return *it->second;
}

const rcsc::Line2D& WorldModel::endLine(Side side)
{
    return side == SIDE_LEFT ? M_left_end_line : M_right_end_line;
}

const rcsc::Line2D& WorldModel::sideLine(Side side)
{
    return side == SIDE_LEFT ? M_left_side_line : M_right_side_line;
}

const rcsc::Line2D& WorldModel::goalLine(Side side)
{

}

const rcsc::Line2D& WorldModel::midfieldLine()
{

}


}
}
