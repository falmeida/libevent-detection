#ifndef BALL_HOLD_DETECTOR_H
#define BALL_HOLD_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <ball_hold.h>

namespace soccer 
{
	namespace event 
	{
        class BallHoldDetector
                : public IObservable<BallHold>
                , public IObserver<rcsc::rcg::DispInfoT> {
		public:
			BallHoldDetector(double min_hold_time, double min_move_distance);

            void update(const rcsc::rcg::DispInfoT& scene);
		private:
            rcsc::rcg::DispInfoT* M_first_scene;
            rcsc::rcg::PlayerT* M_ball_holder;

			double M_min_hold_time;
			double M_min_move_distance;
		};

	}
}
#endif // BALL_HOLD_DETECTOR_H
