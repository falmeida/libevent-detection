#ifndef SHOOT_DETECTOR_H
#define SHOOT_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>

#include <ball_kick.h>

#include <shoot.h>
#include <goal.h>
#include <interception.h>

namespace soccer
{
	namespace event
	{
	
        class ShootDetector
            : public IObservable<Shoot>
            , public IObserver<BallKick>
            , public IObserver<rcsc::rcg::DispInfoT>
        {
		public:
            ShootDetector();
			
            void update(const BallKick& ball_kick);
            void update(const rcsc::rcg::DispInfoT& scene);
        private:
            BallKick* M_ball_kick;
		};

	}
}


#endif
