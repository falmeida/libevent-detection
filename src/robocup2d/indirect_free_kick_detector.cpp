#include "indirect_free_kick_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer
{
    namespace event
    {


        IndirectFreeKickDetector::IndirectFreeKickDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void IndirectFreeKickDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_IndFreeKick_Left:
                {
                        IndirectFreeKick free_kick_left(M_last_playmode_time
                                                    , scene.show_.time_
                                                    , SIDE_LEFT);
                        notifyObservers( free_kick_left );
                 }
                    break;
                    case PM_IndFreeKick_Right:
                {
                        IndirectFreeKick free_kick_right(M_last_playmode_time
                                                     , scene.show_.time_
                                                     , SIDE_RIGHT);
                        notifyObservers( free_kick_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }
	}
}
