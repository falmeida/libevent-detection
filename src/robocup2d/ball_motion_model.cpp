#include "ball_motion_model.h"

#include "robocup2d_scene_player.h"
#include <rcsc/math_util.h>
#include <rcsslogplayer/types.h>


using namespace rcsc;
using namespace rcss::rcg;

namespace soccer {


BallMotionModel::BallMotionModel()
{

}

Ball BallMotionModel::freeMotion(const Ball& ball, unsigned int time)
{
    const ServerParamT& SP = RoboCup2DScenePlayer::instance().serverParams();

    // Calculate the new position
    double x = ball.position().x + calc_sum_geom_series(ball.velocity().x, SP.ball_decay_, time);
    double y = ball.position().y + calc_sum_geom_series(ball.velocity().y, SP.ball_decay_, time);
    // Calculate the new velocity
    double vx = ball.velocity().x * pow(ball.velocity().x, time);
    double vy = ball.velocity().y * pow(ball.velocity().y, time);

    return Ball(Vector2D(x, y), Vector2D(vx, vy));
}


Ball BallMotionModel::freeMotion(const Ball& ball)
{
    const ServerParamT& SP = RoboCup2DScenePlayer::instance().serverParams();

    // Calculate the new position
    double x = ball.position().x + calc_sum_inf_geom_series(ball.velocity().x, SP.ball_decay_);
    double y = ball.position().y + calc_sum_inf_geom_series(ball.velocity().y, SP.ball_decay_);
    // Calculate the new velocity

    return Ball(Vector2D(x, y), Vector2D(0.0, 0.0));
}

}
