#ifndef INDIRECT_FREE_KICK_DETECTOR_H
#define INDIRECT_FREE_KICK_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <indirect_free_kick.h>

namespace soccer
{
	namespace event
	{
	
        class IndirectFreeKickDetector
            : public IObservable<IndirectFreeKick>
            , public IObserver<rcsc::rcg::DispInfoT> {
		public:
			IndirectFreeKickDetector();

            void update(const rcsc::rcg::DispInfoT& scene);

        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;

		};

	}
}


#endif
