#include "ball_dispute_detector.h"

using namespace std;

namespace soccer {
namespace event {

BallDisputeDetector::BallDisputeDetector()
{

}

void BallDisputeDetector::update(const BallKick& ball_kick)
{
    // Multiple ball kicks can be detected at the same time
    M_ball_kicks.push_back(ball_kick);
}

void BallDisputeDetector::update(const rcsc::rcg::DispInfoT& scene)
{
    if ( M_ball_kicks.empty() )
    {
        return;
    }

    if ( M_ball_kicks.size() > 1 )
    {
        const BallKick& last_ball_kick = M_ball_kicks.at(M_ball_kicks.size()-1);
        if ( last_ball_kick.startTime() < scene.show_.time_ )
        {
            vector<PlayerId> players;
            for ( vector<BallKick>::const_iterator cit = M_ball_kicks.begin();
                  cit != M_ball_kicks.end();
                  cit++)
            {
                const BallKick& ball_kick = *cit;
                players.push_back(ball_kick.kicker());
            }

            BallDispute ball_dispute(last_ball_kick.startTime()
                                     , last_ball_kick.endTime()
                                     , players);
            notifyObservers( ball_dispute );
        }
    }

    // Reset internal structures
    M_ball_kicks.clear();
}

}
}
