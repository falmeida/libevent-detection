#include "offside_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer
{
    namespace event
    {


        OffsideDetector::OffsideDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void OffsideDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_OffSide_Left:
                {
                        Offside offside_left(scene.show_.time_, SIDE_LEFT);
                        notifyObservers( offside_left );
                 }
                    break;
                    case PM_OffSide_Right:
                {
                        Offside offside_right(scene.show_.time_, SIDE_RIGHT);
                        notifyObservers( offside_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }

	}
}
