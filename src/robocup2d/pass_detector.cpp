#include "pass_detector.h"

#include <cstddef> // Issues with NULL identifier (why doesn't this happen in dribble_detector.h)

namespace soccer
{
    namespace event
    {

        PassDetector::PassDetector()
            : M_last_ball_kick( NULL )
        {

        }

        void PassDetector::update(const BallKick& ball_kick)
        {
            if ( M_last_ball_kick == NULL )
            {
                M_last_ball_kick = new BallKick(ball_kick);
                return;
            }

            // Same side and different jersey number
            if ( M_last_ball_kick->kicker().side() == ball_kick.kicker().side() &&
                 M_last_ball_kick->kicker().number() != ball_kick.kicker().number())
            {
                Pass pass(M_last_ball_kick->startTime()
                         , ball_kick.startTime()
                         , M_last_ball_kick->kicker()
                         , ball_kick.kicker());
                notifyObservers( pass );
            }

            delete M_last_ball_kick;
            M_last_ball_kick = new BallKick(ball_kick);
        }

	}
}
