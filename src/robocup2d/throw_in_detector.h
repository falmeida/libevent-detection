#ifndef THROW_IN_DETECTOR_H
#define THROW_IN_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <throw_in.h>

namespace soccer
{
	namespace event
	{
		
        class ThrowInDetector
                : public IObservable<ThrowIn>
                , public IObserver<rcsc::rcg::DispInfoT> {
		public:
            ThrowInDetector();

            void update(const rcsc::rcg::DispInfoT& scene);

        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;

		};

	}
}

#endif
