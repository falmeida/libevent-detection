#ifndef GOAL_KICK_DETECTOR_H
#define GOAL_KICK_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <goal_kick.h>
#include <rcsc/rcg/types.h>


namespace soccer
{
	namespace event
	{
	
        class GoalKickDetector
                : public IObservable<GoalKick>
                , public IObserver<rcsc::rcg::DispInfoT> {
		public:
			GoalKickDetector();

            void update(const rcsc::rcg::DispInfoT& scene);
        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;

		};

	}
}


#endif
