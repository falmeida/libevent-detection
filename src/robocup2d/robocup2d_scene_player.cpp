#include "robocup2d_scene_player.h"


namespace rcss {
namespace rcg {


    RoboCup2DScenePlayer::RoboCup2DScenePlayer()
        : rcss::rcg::Handler()
        , M_time( 0 )
        , M_stopped_time( 0 )
        , M_total_time( 0 )
        , M_pmode( PM_Null )
        , M_current_scene_index(0)
        , M_stopped(true)
        , M_paused(true)
    {

    }

    RoboCup2DScenePlayer::~RoboCup2DScenePlayer()
    {

    }

    RoboCup2DScenePlayer& RoboCup2DScenePlayer::instance()
    {
        static RoboCup2DScenePlayer the_instance;

        return the_instance;
    }


    rcss::rcg::PlayMode
    RoboCup2DScenePlayer::play_mode_id( const char * mode )
    {
        static const char * playmode_strings[] = PLAYMODE_STRINGS;

        for ( int n = 0; n < rcss::rcg::PM_MAX; ++n )
        {
            if ( ! strcmp( playmode_strings[n], mode ) )
            {
                return static_cast< rcss::rcg::PlayMode >( n );
            }
        }
        return rcss::rcg::PM_Null;
    }

    void RoboCup2DScenePlayer::doHandleLogVersion( int ver )
    {
        M_version = ver;
    }

    int RoboCup2DScenePlayer::doGetLogVersion() const
      {
          return M_version;
      }

    void RoboCup2DScenePlayer::doHandleEOF()
    {

    }

    void RoboCup2DScenePlayer::doHandleServerParam( const ServerParamT & param )
    {
        M_server_params = param;
    }

    void RoboCup2DScenePlayer::doHandlePlayerParam( const PlayerParamT & param )
      {
        M_player_params = param;
      }


    void RoboCup2DScenePlayer::doHandlePlayerType( const PlayerTypeT & type )
      {
        M_player_types[type.id_] = type;
      }


    void RoboCup2DScenePlayer::updateTime( int time )
    {
        if ( M_time != time )
        {
            M_time = time;
            M_stopped_time = 0;
        }
        else
        {
            M_stopped_time++;
        }

        M_total_time++;
    }


    void RoboCup2DScenePlayer::doHandleShowInfo( const ShowInfoT & info )
    {
        updateTime ( info.time_ );

        DispInfoT* disp_info = new DispInfoT();

        disp_info->pmode_ = M_pmode;
        disp_info->team_[0] = M_team_left;
        disp_info->team_[1] = M_team_right;
        disp_info->show_ = info;
    }

    void RoboCup2DScenePlayer::doHandlePlayMode( const int,
                                               const PlayMode pmode )
    {
          M_pmode = pmode;
    }

    void RoboCup2DScenePlayer::doHandleTeamInfo( const int,
                                               const TeamT & left,
                                               const TeamT & right )
    {
        M_team_left = left;
        M_team_right = right;
    }


    /// OVERRIDE soccer::Robocup2DScenePlayer interface

    void RoboCup2DScenePlayer::stop()
    {
        M_stopped = true;
        M_current_scene_index = 0;
    }

    void RoboCup2DScenePlayer::pause()
    {
        M_paused = true;
    }

    void RoboCup2DScenePlayer::play()
    {
        M_stopped = M_paused = false;

        while ( !M_stopped && M_paused && M_current_scene_index < M_scenes.size() )
        {
            notifyObservers(*M_scenes.at(M_current_scene_index++));
        }
    }

    const DispInfoT* RoboCup2DScenePlayer::forward(unsigned int step)
    {
        // Pre-condition
        assert ( M_current_scene_index >= 0 && M_current_scene_index < M_scenes.size());

        M_current_scene_index += step;

        // Post-condition
        assert ( M_current_scene_index >= 0 && M_current_scene_index < M_scenes.size());
    }

    const DispInfoT* RoboCup2DScenePlayer::rewind(unsigned int step)
    {
        // Pre-condition
        assert ( M_current_scene_index >= 0 && M_current_scene_index < M_scenes.size());

        M_current_scene_index -= step;

        // Post-condition
        assert ( M_current_scene_index >= 0 && M_current_scene_index < M_scenes.size());
    }

    void RoboCup2DScenePlayer::goToScene(unsigned int index)
    {
        assert (hasScene(index));

        M_current_scene_index = index;
    }

    const DispInfoT* RoboCup2DScenePlayer::currentScene() const
    {
        return M_scenes.at(M_current_scene_index);
    }

    unsigned int RoboCup2DScenePlayer::currentindex() const
    {
        return M_current_scene_index;
    }

    bool RoboCup2DScenePlayer::isPaused() const
    {
        return M_paused;
    }

    bool RoboCup2DScenePlayer::isLastScene() const
    {
        return M_current_scene_index == M_scenes.size() - 1;
    }

    bool RoboCup2DScenePlayer::isFirstScene() const
    {
        return M_current_scene_index == 0;
    }

    bool RoboCup2DScenePlayer::hasScene(unsigned int index) const
    {
        return index >= 0 && index < M_scenes.size();
    }

    const ServerParamT &RoboCup2DScenePlayer::serverParams() const
    {
        return M_server_params;
    }

    const PlayerParamT& RoboCup2DScenePlayer::playerParams() const
    {
        return M_player_params;
    }

    const PlayerTypeT& RoboCup2DScenePlayer::playerType(unsigned int type) const
    {
        return M_player_types.at(type);
    }


} // end namespace rcg
} // end namespace rcss
