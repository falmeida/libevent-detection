#ifndef OFFSIDE_DETECTOR_H
#define OFFSIDE_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <offside.h>

namespace soccer
{
	namespace event
	{
		
        class OffsideDetector
            : public IObservable<Offside>
            , public IObserver<rcsc::rcg::DispInfoT>
        {
		public:
			OffsideDetector();
			
            void update(const rcsc::rcg::DispInfoT& scene);
        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;
		};
	
	}
}

#endif
