#ifndef CORNER_KICK_DETECTOR_H
#define CORNER_KICK_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <corner_kick.h>
#include <ball_kick.h>
#include <rcsc/rcg/types.h>

namespace soccer
{
    namespace event
    {
	
        class CornerKickDetector
                        : public IObservable<CornerKick>
                        , public IObserver<rcsc::rcg::DispInfoT>
                        , public IObserver<BallKick>
        {
        public:
            CornerKickDetector();

            void update(const rcsc::rcg::DispInfoT& scene);
            void update(const BallKick& ball_kick);

        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;
            BallKick* M_corner_ball_kick;
		};

	}
}


#endif
