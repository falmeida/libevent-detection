#ifndef FREE_KICK_DETECTOR_H
#define FREE_KICK_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <free_kick.h>

namespace soccer
{
	namespace event
	{
	
        class FreeKickDetector
                : public IObservable<FreeKick>
                , public IObserver<rcsc::rcg::DispInfoT>{
		public:
			FreeKickDetector();
			
            void update(const rcsc::rcg::DispInfoT& scene);

        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;
		};

	}
}


#endif
