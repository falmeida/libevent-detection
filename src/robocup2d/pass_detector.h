#ifndef PASS_DETECTOR_H
#define PASS_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>

#include <pass.h>
#include <ball_kick.h>

namespace soccer
{
	namespace event
	{
		
        class PassDetector
            : public IObservable<Pass>
            , public IObserver<BallKick>
        {
		public:
			PassDetector();
			
            void update(const BallKick& scene);

        private:
            BallKick* M_last_ball_kick;
		};
	
	}
}

#endif
