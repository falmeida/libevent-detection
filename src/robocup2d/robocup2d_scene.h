#ifndef ROBOCUP2D_SCENE_H
#define ROBOCUP2D_SCENE_H

#include <iscene.h>
#include "robocup2d_scene_player.h"

#include <rcsslogplayer/types.h>

#include <vector>
#include <map>
#include <cassert>

namespace rcss {
namespace rcg {

    struct PlayerDistanceToPointComparator {

    public:
        PlayerDistanceToPointComparator(rcsc::Vector2D point)
            : M_point(point)
        {

        }

        bool operator() (const PlayerT* p1, const PlayerT* p2)
        {
            assert ( p1 != NULL && p2 != NULL );

            // Could cache the results
            calculatePlayerDistanceToPoint(p1);
            calculatePlayerDistanceToPoint(p2);

            return M_distances.at(p1) < M_distances.at(p2);
        }
    protected:
        void calculatePlayerDistanceToPoint(const PlayerT* p)
        {
            if ( M_distances.find(p) == M_distances.end() )
            {
                M_distances.insert(std::pair<const PlayerT*, double>(p, M_point.dist(rcsc::Vector2D(p->x_, p->y_))));
            }
        }

    private:
      rcsc::Vector2D M_point;
      std::map<const PlayerT*, float> M_distances;
    };

    struct PlayerPosXComparator {
      bool operator() (const PlayerT* p1, const PlayerT* p2)
      {
          assert ( p1 != NULL && p2 != NULL );

          return p1->x_< p2->x_ ;
      }
    } playerPosXComparator;

    class Scene // : public soccer::IScene
    {
    public:
        Scene(rcss::rcg::DispInfoT* scene, const RoboCup2DScenePlayer* scene_player);

        /// Interface implementation
        float defenseLine(Side side);
        float offenseLine(Side side);

        const PlayerT* goalie(Side side);

        soccer::Ball ball();

        unsigned int numPlayers();

        const PlayerT* ballOwner();
        /*!
          \brief Check if the ball was kicked in this scene
          \return True if the ball was kicked in the scene or false otherwise
        */
        bool ballKicked();

        vector<PlayerT *> possibleKickers();

        bool ballOutOfBounds();
        bool isGameStopped();

        std::vector<PlayerT*> teammatesFromPlayer(const PlayerT* player);
        std::vector<PlayerT*> opponentsFromPlayer(const PlayerT* player);

        bool canKickBall(const PlayerT* player) const;
        bool canCatchBall(const PlayerT* player) const;
        bool canTackleBall(const PlayerT* player) const;
        bool isKickingBall(const PlayerT* player) const;

    private:
        const RoboCup2DScenePlayer* M_scene_player;



        // Precalculated
        rcss::rcg::PlayerT* M_left_goalie;
        rcss::rcg::PlayerT* M_left_last_defender;
        rcss::rcg::PlayerT* M_left_last_attacker;
        rcss::rcg::PlayerT* M_right_goalie;
        rcss::rcg::PlayerT* M_right_last_defender;
        rcss::rcg::PlayerT* M_right_last_attacker;

        std::vector<rcss::rcg::PlayerT*> M_left_players;
        std::vector<rcss::rcg::PlayerT*> M_right_players;

        std::vector<rcss::rcg::PlayerT*> M_possible_kickers;

        /// Players sorted by distance from ball
        std::vector<rcss::rcg::PlayerT*> M_left_players_from_ball;
        std::vector<rcss::rcg::PlayerT*> M_right_players_from_ball;

        soccer::Ball M_ball;
        const rcss::rcg::DispInfoT* M_scene;
    };


}
}

#endif
