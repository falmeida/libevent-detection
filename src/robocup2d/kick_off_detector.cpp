#include "kick_off_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer
{
	namespace event
	{

        KickOffDetector::KickOffDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void KickOffDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_KickOff_Left:
                {
                        KickOff kick_off_left(M_last_playmode_time
                                              , scene.show_.time_
                                              , SIDE_LEFT);
                        notifyObservers( kick_off_left );
                }
                        break;
                    case PM_KickOff_Right:
                {
                        KickOff kick_off_right(M_last_playmode_time
                                               , scene.show_.time_
                                               , SIDE_RIGHT);
                        notifyObservers( kick_off_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }

	}
}
