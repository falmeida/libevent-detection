#ifndef KICK_OFF_DETECTOR_H
#define KICK_OFF_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <kick_off.h>

namespace soccer
{
	namespace event 
	{		
        class KickOffDetector
            : public IObservable<KickOff>
            , public IObserver<rcsc::rcg::DispInfoT> {
		public:
			KickOffDetector();
		
           void update(const rcsc::rcg::DispInfoT& scene);
        private:
           unsigned int M_last_playmode_time;
           rcsc::PlayMode M_last_playmode;
		};

	}
}


#endif
