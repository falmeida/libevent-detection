#include "goal_kick_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer 
{
	namespace event 
	{

        GoalKickDetector::GoalKickDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void GoalKickDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_GoalKick_Left:
                {
                        GoalKick goal_kick_left(M_last_playmode_time
                                              , scene.show_.time_
                                              , SIDE_LEFT);
                        notifyObservers( goal_kick_left );
                }
                        break;
                    case PM_GoalKick_Right:
                {
                        GoalKick goal_kick_right(M_last_playmode_time
                                               , scene.show_.time_
                                               , SIDE_RIGHT);
                        notifyObservers( goal_kick_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }

	}
}
