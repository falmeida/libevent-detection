#include "throw_in_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer
{
    namespace event
    {

        ThrowInDetector::ThrowInDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void ThrowInDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_KickIn_Left:
                {
                        ThrowIn throw_in_left(M_last_playmode_time
                                              , scene.show_.time_
                                              , SIDE_LEFT);
                        notifyObservers( throw_in_left );
                }
                        break;
                    case PM_KickIn_Right:
                {
                        ThrowIn throw_in_right(M_last_playmode_time
                                               , scene.show_.time_
                                               , SIDE_RIGHT);
                        notifyObservers( throw_in_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }

    }
}
