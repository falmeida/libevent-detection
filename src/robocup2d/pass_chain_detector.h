#ifndef PASS_CHAIN_DETECTOR_H
#define PASS_CHAIN_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>

#include <pass_chain.h>

#include <pass.h>
#include <dribble.h>
#include <ball_hold.h>

namespace soccer
{
	namespace event
	{

        class PassChainDetector
                        : public IObservable<PassChain>
                        , public IObserver<Pass>
                        , public IObserver<Dribble>
                        , public IObserver<BallHold>
        {
		public:
			PassChainDetector();
		
            void update(const Pass& pass);
            void update(const Dribble& dribble);
            void update(const BallHold& dribble);

		};
	
	}
}

#endif
