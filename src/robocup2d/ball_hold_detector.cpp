#include "ball_hold_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer
{
	namespace event
	{

		BallHoldDetector::BallHoldDetector(double min_hold_time, double min_move_distance)
			: M_min_hold_time(min_hold_time)
			, M_min_move_distance(min_move_distance)
		{
            M_first_scene = NULL;
            M_ball_holder = NULL;
		}

        void BallHoldDetector::update(const DispInfoT &scene)
		{
            // BallHold starts with the ball being kickable
            if ( M_first_scene != NULL )
            {
                // Is the ball still kickable by the same player in the current scene


            }
		}

	}
}
