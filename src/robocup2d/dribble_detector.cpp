#include "dribble_detector.h"

namespace soccer 
{
	namespace event 
	{

        DribbleDetector::DribbleDetector()
            : M_last_ball_kick(NULL)
		{
		
		}
	
        void DribbleDetector::update(const BallKick& ball_kick)
		{
            if ( M_last_ball_kick == NULL )
            {
                M_last_ball_kick = new BallKick(ball_kick);
                return;
            }

            if ( M_last_ball_kick->kicker() == ball_kick.kicker() )
            {
                Dribble dribble(M_last_ball_kick->startTime()
                                , ball_kick.startTime()
                                , ball_kick.kicker());
                notifyObservers( dribble );
            }

            delete M_last_ball_kick;
            M_last_ball_kick = new BallKick(ball_kick);
		}

	}
}
