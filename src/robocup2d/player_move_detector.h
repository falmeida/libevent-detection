#ifndef PLAYER_MOVE_DETECTOR_H
#define PLAYER_MOVE_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsslogplayer/types.h>

namespace soccer
{
	namespace event
	{
		
                class PlayerMoveDetector
                        : public IObservable<IEvent>
                        , public IObserver<rcss::rcg::DispInfoT> {
		public:
			PlayerMoveDetector(double min_move_distance);
					   			
                        void update(rcss::rcg::DispInfoT& scene);
		
		private:
			double M_min_move_distance;
		};
	
	}
}

#endif
