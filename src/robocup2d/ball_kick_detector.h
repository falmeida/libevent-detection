#ifndef BALL_KICK_DETECTOR_H
#define BALL_KICK_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>
#include <ievent.h>

#include <rcsc/rcg/types.h>

#include <vector>

namespace soccer {
namespace event {

    class BallKickDetector
            : public IObservable<IEvent>
            , public IObserver<rcsc::rcg::DispInfoT>
    {
    public:
        BallKickDetector();

        void update(const rcsc::rcg::DispInfoT& scene);

    public:
        bool canKick(const rcsc::rcg::PlayerT& player, const rcsc::rcg::BallT& ball);
        bool canCatch(const rcsc::rcg::PlayerT& player, const rcsc::rcg::BallT& ball);
        bool canTackle(const rcsc::rcg::PlayerT& player, const rcsc::rcg::BallT& ball);

    private:
        const rcsc::rcg::DispInfoT* M_last_scene;
        std::vector<rcsc::rcg::PlayerT*> M_last_possible_kickers;
    };

}
}

#endif
