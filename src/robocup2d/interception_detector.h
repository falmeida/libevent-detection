#ifndef INTERCEPTION_DETECTOR_H
#define INTERCEPTION_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <ball_kick.h>

namespace soccer
{
    namespace event
    {
        class InterceptionDetector
                : public IObservable<IEvent>
                , public IObserver<BallKick> {
        public:
            InterceptionDetector();

            void update(const BallKick& scene);
        private:
            BallKick* M_last_ball_kick;
        };

    }
}
#endif // INTERCEPTION_DETECTOR_H
