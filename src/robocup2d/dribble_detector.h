#ifndef DRIBBLE_DETECTOR_H
#define DRIBBLE_DETECTOR_H

#include <iobservable.h>
#include <iobserver.h>

#include <dribble.h>
#include <ball_kick.h>

namespace soccer 
{
	namespace event
	{
		
        class DribbleDetector
            : public IObservable<Dribble>
            , public IObserver<BallKick> {
		public:
            DribbleDetector();

            void update(const BallKick& ball_kick);
		private:
            BallKick* M_last_ball_kick;
		};

	}
}

#endif
