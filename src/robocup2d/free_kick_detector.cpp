#include "free_kick_detector.h"

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer 
{
	namespace event 
	{


        FreeKickDetector::FreeKickDetector()
            : M_last_playmode_time(0u)
            , M_last_playmode(PM_Null)
        {

        }

        void FreeKickDetector::update(const DispInfoT& scene)
        {
            if ( M_last_playmode != scene.pmode_)
            {
                switch ( M_last_playmode )
                {
                    case PM_FreeKick_Left:
                {
                        FreeKick free_kick_left(M_last_playmode_time
                                                    , scene.show_.time_
                                                    , SIDE_LEFT);
                        notifyObservers( free_kick_left );
                 }
                    break;
                    case PM_FreeKick_Right:
                {
                        FreeKick free_kick_right(M_last_playmode_time
                                                     , scene.show_.time_
                                                     , SIDE_RIGHT);
                        notifyObservers( free_kick_right );
                }
                        break;
                }

                M_last_playmode_time = scene.show_.time_;
                M_last_playmode = scene.pmode_;
            }
        }

	}
}
