#include "interception_detector.h"

namespace soccer
{
    namespace event
    {

        InterceptionDetector::InterceptionDetector()
            : M_last_ball_kick(NULL)
        {

        }

        void InterceptionDetector::update(const BallKick& ball_kick)
        {
            if ( M_last_ball_kick == NULL )
            {
                M_last_ball_kick = new BallKick(ball_kick);
                return;
            }

            // Consider possibly multiple kickers
            if ( M_last_ball_kick->kicker().side() != ball_kick.kicker().side() )
            {
                Dribble dribble(M_last_ball_kick->startTime()
                                , ball_kick.startTime()
                                , ball_kick.kicker());
                notifyObservers( dribble );
            }

            delete M_last_ball_kick;
            M_last_ball_kick = new BallKick(ball_kick);
        }

    }
}
