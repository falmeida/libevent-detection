#include "shoot_detector.h"

#include "ball_motion_model.h"
#include "world_model.h"

#include <rcsc/geom.h>
#include <ball.h>

using namespace rcsc;
using namespace rcsc::rcg;

namespace soccer 
{
	namespace event
	{
		
        ShootDetector::ShootDetector()
            : M_ball_kick(NULL)
		{

		}


        void ShootDetector::update(const BallKick& ball_kick)
        {
            if ( M_ball_kick == NULL )
            {
                M_ball_kick = new BallKick(ball_kick);
            }

        }

        void ShootDetector::update(const DispInfoT& scene)
        {
            if ( M_ball_kick != NULL )
            {
                // Check if ball has enough strength to reach the goal
                Ball ball_start(Vector2D(scene.show_.ball_.x_, scene.show_.ball_.y_),
                                Vector2D(scene.show_.ball_.vx_, scene.show_.ball_.vy_));

                BallMotionModel ball_motion_model;
                Ball ball_end = ball_motion_model.freeMotion(ball_start);

                Line2D ball_path(ball_start, ball_end);


            }

        }

	}
}

