#include "robocup2d_scene.h"

#include <algorithm>

using namespace soccer;
using namespace rcsc;
using namespace rcss::rcg;
using namespace std;

namespace rcss {
namespace rcg {

    Scene::Scene(rcss::rcg::DispInfoT* scene, const RoboCup2DScenePlayer* scene_player)
        : M_scene_player(scene_player)
        , M_ball(Vector2D(scene->show_.ball_.x_, scene->show_.ball_.y_),
                 Vector2D(scene->show_.ball_.vx_, scene->show_.ball_.vy_))
    {
        M_scene = scene;

        M_left_goalie = M_right_goalie = NULL;
        M_left_last_defender = M_right_last_defender = NULL;
        M_left_last_attacker = M_right_last_attacker = NULL;

        for ( unsigned int i = 0; i < numPlayers(); i++)
        {
            M_left_players.push_back(&scene->show_.player_[i]);
        }
        M_left_players_from_ball.insert(M_left_players_from_ball.begin(),
                                        M_left_players.begin(),
                                        M_left_players.end());

        for ( unsigned int i = numPlayers(); i < numPlayers() * 2; i++)
        {
            M_right_players.push_back(&scene->show_.player_[i]);
        }
        M_right_players_from_ball.insert(M_right_players_from_ball.begin(),
                                         M_right_players.begin(),
                                         M_right_players.end());

        PlayerDistanceToPointComparator player_to_ball_comparator(Vector2D(scene->show_.ball_.x_,
                                                                           scene->show_.ball_.y_));

        /// Sort team players based on their distance to the ball
        sort( M_left_players_from_ball.begin(), M_left_players_from_ball.end(), player_to_ball_comparator);
        sort( M_right_players_from_ball.begin(), M_right_players_from_ball.end(), player_to_ball_comparator);

        /// Sort team players based on their X-distance to the opponent goal
        sort( M_left_players.begin(), M_left_players.end(), playerPosXComparator);
        sort( M_right_players.begin(), M_right_players.end(), playerPosXComparator);
        reverse( M_right_players.begin(), M_right_players.end() );

        /// Find the last defender and goalie of the left team
        vector<PlayerT*>::iterator it_player = M_left_players.begin();
        while ( ( M_left_goalie == NULL || M_left_last_defender == NULL ) && it_player != M_left_players.end() )
        {
            if ( (*it_player)->isGoalie() )
            {
                M_left_goalie = *it_player;
            }
            else
            {
                M_left_last_defender = *it_player;
            }
            it_player++;
        }

        /// Find the last defender and goalie of the right team
        it_player = M_right_players.begin();
        while ( ( M_right_goalie == NULL || M_right_last_defender == NULL ) && it_player != M_right_players.end() )
        {
            if ( (*it_player)->isGoalie() )
            {
                M_right_goalie = *it_player;
            }
            else
            {
                M_right_last_defender = *it_player;
            }
            it_player++;
        }

        /// Find the last attacker of the left and right team
        M_right_last_attacker = M_right_players.front();
        M_left_last_attacker = M_left_players.back();

        /// Find possible kickers from both teams
        it_player = M_left_players_from_ball.begin();

        while ( it_player != M_left_players_from_ball.end() && canKickBall(*it_player) )
        {
            M_possible_kickers.push_back(*it_player);
        }

        it_player = M_right_players_from_ball.begin();
        while ( it_player != M_right_players_from_ball.end() && canKickBall(*it_player) )
        {
            M_possible_kickers.push_back(*it_player);
        }
        sort( M_possible_kickers.begin(), M_possible_kickers.end(), player_to_ball_comparator );

    }


    bool Scene::canKickBall(const PlayerT* player) const
    {
        const PlayerTypeT& PT = M_scene_player->playerType(player->type_);
        const ServerParamT SP = M_scene_player->serverParams();;
        return Vector2D(player->x_, player->y_).dist(M_ball.position()) <= (SP.ball_size_ +
                                                                            PT.player_size_ +
                                                                            PT.kickable_margin_);
    }

    bool Scene::canCatchBall(const PlayerT* player) const
    {
        const PlayerTypeT& PT = M_scene_player->playerType(player->type_);
        const ServerParamT& SP = M_scene_player->serverParams();

        return Vector2D(player->x_, player->y_).dist(M_ball.position()) <= (SP.catchable_area_l_ +
                                                                            PT.catchable_area_l_stretch_);
    }

    bool Scene::canTackleBall(const PlayerT* player) const
    {
        // Vector2D rb = toRelative(ball);
        // return (rb.getX() > 0 && rb.getX() <= Param.TACKLE_DIST && Math.abs(rb.getY()) <= Param.TACKLE_WIDTH / 2);
        return false;
    }

    bool Scene::isKickingBall(const PlayerT* player) const
    {
        return player->isKicking() &&
               find(M_possible_kickers.begin(), M_possible_kickers.end(), player) != M_possible_kickers.end();
    }

    /// Interface implementation
    float Scene::defenseLine(Side side)
    {
        return side == LEFT ?
                    M_left_last_defender->x_
                  : M_right_last_defender->x_;
    }

    float Scene::offenseLine(Side side)
    {
        return side == LEFT ?
                    M_left_last_attacker->x_
                  : M_right_last_attacker->x_;
    }

    const PlayerT* Scene::goalie(Side side)
    {
        return side == LEFT ?
                    M_left_goalie
                  : M_right_goalie;
    }

    Ball Scene::ball()
    {
        return M_ball;
    }


    unsigned int Scene::numPlayers()
    {
        return 11;
    }

    const PlayerT* Scene::ballOwner()
    {
        if ( M_possible_kickers.empty() )
        {
            /// TODO Ball owner shall be the fastest player to the ball
            return NULL;
        }

        return M_possible_kickers[0];
    }

    bool Scene::ballKicked()
    {
        if ( possibleKickers().empty() )
        {
            return false;
        }

        PlayerT* possible_ball_kicker = M_possible_kickers[0];
        return possible_ball_kicker->isKicking();
    }

    vector<PlayerT*> Scene::possibleKickers()
    {
        return M_possible_kickers;
    }

    bool Scene::ballOutOfBounds()
    {

    }

    bool Scene::isGameStopped()
    {

    }

    std::vector<PlayerT*> Scene::teammatesFromPlayer(const PlayerT* player)
    {
        struct PlayerDistanceToPointComparator teammatesDistanceToPlayer(Vector2D(player->x_, player->y_));

        vector<PlayerT*> teammates( player->side() == LEFT ? M_left_players : M_right_players);
        /// Sort teammates distance to self in ascending
        sort( teammates.begin(), teammates.end(), teammatesDistanceToPlayer );
        /// Remove pointer to self
        teammates.erase( teammates.begin() );

        return teammates;
    }

    std::vector<PlayerT*> Scene::opponentsFromPlayer(const PlayerT* player)
    {
        PlayerDistanceToPointComparator teammatesDistanceToPlayer(Vector2D(player->x_, player->y_));

        vector<PlayerT*> opponents( player->side() == LEFT ? M_right_players : M_left_players);
        /// Sort oppnents distance to self in ascending
        sort( opponents.begin(), opponents.end(), teammatesDistanceToPlayer );

        return opponents;
    }

}
}
