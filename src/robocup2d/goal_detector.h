#ifndef GOAL_DETECTOR_H
#define GOAL_DETECTOR_H

#include <ievent.h>
#include <iobservable.h>
#include <iobserver.h>

#include <rcsc/rcg/types.h>
#include <goal.h>

namespace soccer
{
	namespace event 
	{

        class GoalDetector
            : public IObservable<Goal>
            , public IObserver<rcsc::rcg::DispInfoT>
        {
		public:
            GoalDetector();

            void update(const rcsc::rcg::DispInfoT& scene);
        private:
            unsigned int M_last_playmode_time;
            rcsc::PlayMode M_last_playmode;
		};

	}
}

#endif
